import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {AccountInfoPage} from '../account-info/account-info';
import {ActionSheetController} from 'ionic-angular';


@Component({
  selector: 'page-newacc',
  templateUrl: 'newacc.html'
})
export class newaccpage {
  firstname:any;
  lastname:any;
  email:any;
  password:any;
  confirmpassword:any;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController) {




  }
 public submit()
  {
     let alert = this.alertCtrl.create({
      title: 'Account Create',
      subTitle: 'Account Create Sucessfully',
      buttons: [
        {
          text: 'OK',
          role: 'ok',
          handler: () => {
            console.log(this.firstname);
            this.navCtrl.push(AccountInfoPage,{
              'firstname':this.firstname,
              'lastname':this.lastname,
              'email':this.email,
            });
            
          }
        },
       
      ]
      
    });
    alert.present();
  }

}
