import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { newaccpage } from './newacc';

@NgModule({
  declarations: [
    newaccpage,
  ],
  imports: [
    IonicPageModule.forChild(newaccpage),
  ],
})
export class NewaccPageModule {}
