import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {ForgetPasswordPage} from '../forget-password/forget-password';
import {newaccpage} from '../newacc/newacc';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
username:any;
password:any;
  constructor(public navCtrl: NavController,private alertCtrl: AlertController,private facebook: Facebook) {

  }
  
public login()
{
  if(this.username=="ankita" && this.password=="123"){
    let alert = this.alertCtrl.create({
      title: 'Login',
      subTitle: 'Login Successfully',
      buttons: ['OK']
    });
    alert.present();
  }
  else{
    let alert = this.alertCtrl.create({
      title: 'Login',
      subTitle: 'Username or password is invalid',
      buttons: ['OK']
    });
    alert.present();
  }

}

public forget_password(){
  this.navCtrl.push(ForgetPasswordPage);
}

public create_account(){
  this.navCtrl.push(newaccpage);
}
public fbLogin(){
  this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
    this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
      console.log(profile);
      // this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
    });
  });
}
}
