import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AccountInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-info',
  templateUrl: 'account-info.html',
})
export class AccountInfoPage {
firstname:any;
lastname:any;
email:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.firstname = navParams.get('firstname');
    console.log(this.firstname);
    this.lastname = navParams.get('lastname');
    console.log(this.lastname);
    this.email = navParams.get('email');
    console.log(this.email);


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountInfoPage');
  }

}
