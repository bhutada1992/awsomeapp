import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html',
})
export class ForgetPasswordPage {

  email:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPasswordPage');
  }

  public submit()
  {
  console.log(this.email);
    
    if(this.email=="ankitabafna@gmail.com"){
    let alert = this.alertCtrl.create({
      title: 'Password',
      subTitle: 'password send on email Successfully',
      buttons: ['OK']
    });
    alert.present();
  }
  else{
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'email is invalid',
      buttons: ['OK']
    });
    alert.present();
  }
}

}
